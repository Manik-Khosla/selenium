/**
 * 
 */
package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import selenium.driver.DriverInitializer;

/**
 * @author hp
 *
 */
public class HomePage {
	
	private static String Title;
	private static final String URL = "https://demoqa.com/";
	
	public static void setTitle(String title) {
		Title = title;
	}
	
	public static String getTitle() {
		return Title;
	}
	
	public static String getURL() {
		return URL;
	}
	
	public void gotoHomePage() {
		WebElement image = DriverInitializer.getdriverInstance().findElement(By.xpath("//img[@src = '/images/Toolsqa.jpg'"));
		image.click();
	}
	
	public static void clickCard(WebElement element) {
		element.click();
	}
	
    public static WebElement getElementCard() {
    	return DriverInitializer.getdriverInstance().findElement(By.xpath("//h5[text() = 'Elements']"));
	}
	
    public static WebElement getFormCard() {
    	return DriverInitializer.getdriverInstance().findElement(By.xpath("//h5[text() = 'Forms']"));
	}

    public static WebElement getAlertCard() {
    	return DriverInitializer.getdriverInstance().findElement(By.xpath("//h5[text() = 'Alerts, Frame & Windows']"));
    }

    public static WebElement getWidgetCard() {
    	return DriverInitializer.getdriverInstance().findElement(By.xpath("//h5[text() = 'Widgets']"));
    }

    public static WebElement getInteractionCard() {
    	return DriverInitializer.getdriverInstance().findElement(By.xpath("//h5[text() = 'Interactions']"));
    }

}
