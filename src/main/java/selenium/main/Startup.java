/**
 * 
 */
package selenium.main;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import models.HomePage;
import selenium.driver.DriverInitializer;
import selenium.support.Browsers;

/**
 * @author hp
 *
 */
public class Startup {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Setup driver 
		WebDriver driver = new DriverInitializer("webdriver.chrome.driver" , "C:\\Users\\hp\\Selenium\\chromedriver_win32\\chromedriver.exe", Browsers.CHROME, 30).getdriverInstance();
        driver.get(HomePage.getURL());
        driver.manage().window().maximize();
        HomePage.setTitle(driver.getTitle());
        
        // close Banner
        driver.findElement(By.id("close-fixedban")).click();  
        // scroll down
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,250)", "");
        HomePage.clickCard(HomePage.getElementCard());
        
        
	}

}
