/**
 * 
 */
package selenium.driver;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import selenium.support.Browsers;

/**
 * @author hp
 *
 */
public class DriverInitializer{
	
	private static WebDriver driver;
	private String browserName;
	private final int DEFAULT_TIMEOUT = 30;
	
	
	public DriverInitializer( String key, String executablePath, Browsers browser, int Timeout) {
		System.setProperty(key, executablePath);
		
		switch(browser){
			case CHROME:
				driver = new ChromeDriver();
				this.browserName = browser.CHROME.toString();
				break;
			case FIREFOX:
				driver = new FirefoxDriver();
				this.browserName = browser.FIREFOX.toString();
				break;
			case EDGE:
				driver = new EdgeDriver();
				this.browserName = browser.EDGE.toString();
				break;
		}
		
		driver.manage().timeouts().implicitlyWait(Timeout, TimeUnit.SECONDS);
	}
	
	public DriverInitializer(Browsers chrome) {
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
	}

	/**
	 * returns driver instance created and initialized for specific browser
	 * @return
	 */
	public static WebDriver getdriverInstance(){
		if(driver == null){
			new DriverInitializer(Browsers.CHROME);
			return driver;
		}else{
			return driver;
		}
	}

	
}
